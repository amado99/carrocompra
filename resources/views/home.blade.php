@extends('layouts.app')

@section('content')
<div class="container">
    <div >
        <lista-producto :producto="{{$producto}}"></lista-producto>
    </div>
</div>
@endsection
