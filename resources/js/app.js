
require('./bootstrap');

window.Vue = require('vue');


Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('lista-producto', require('./components/listaProductos.vue').default);
const app = new Vue({
    el: '#app',
});
