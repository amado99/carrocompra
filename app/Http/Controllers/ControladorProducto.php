<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto as Producto;
use App\Models\Venta as Venta;

class ControladorProducto extends Controller
{
    //
    public function index(){
         $producto = Producto::all();
         return \View::make("listaProducto",compact("producto"));
    }
    public function store(Request $request){
    	$venta = new Venta();
    	$venta->costo=$request->total;
    	$venta->productos=json_encode([$request->productos]);
    	$venta->save();
    }
}
